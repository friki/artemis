Bug-Database: https://github.com/sanger-pathogens/Artemis/issues
Bug-Submit: https://github.com/sanger-pathogens/Artemis/issues/new
Reference:
 - Author: >
    Tim Carver and Simon R. Harris and Matthew Berriman and Julian
    Parkhill and Jacqueline A. McQuillan
   Title: >
    Artemis: an integrated platform for visualization and analysis of
    high-throughput sequence-based experimental data
   Journal: Bioinformatics
   Year: 2012
   Volume: 28
   Number: 4
   Pages: 464-469
   DOI: 10.1093/bioinformatics/btr703
   PMID: 22199388
   URL: http://bioinformatics.oxfordjournals.org/content/28/4/464
   eprint: >
    http://bioinformatics.oxfordjournals.org/content/28/4/464.full.pdf+html
 - Author: >
    Kim Rutherford and Julian Parkhill and James Crook and Terry Horsnell
    and Peter Rice and Marie-Adèle Rajandream and Bart Barrell
   Title: "Artemis: sequence visualization and annotation"
   Journal: Bioinformatics
   Year: 2000
   Volume: 16
   Number: 10
   Pages: 944-945
   DOI: 10.1093/bioinformatics/16.10.944
   PMID: 11120685
   URL: bioinformatics.oxfordjournals.org/content/16/10/94
   eprint: >
    http://bioinformatics.oxfordjournals.org/content/16/10/944.full.pdf+html
 - Author: >
    Tim Carver and Matthew Berriman and Adrian Tivey and Chinmay Patel
    and Ulrike Böhme and Barclay G. Barrell and Julian Parkhill and
    Marie-Adèle Rajandream
   Title: >
    Artemis and ACT: viewing, annotating and comparing sequences stored
    in a relational database
   Journal: Bioinformatics
   Year: 2008
   Volume: 24
   Number: 23
   Pages: 2672-2676
   DOI: 10.1093/bioinformatics/btn529
   PMID: 18845581
   URL: http://bioinformatics.oxfordjournals.org/content/24/23/2672
   eprint: >
    http://bioinformatics.oxfordjournals.org/content/24/23/2672.full.pdf+html
Registry:
 - Name: SciCrunch
   Entry: SCR_004267
 - Name: OMICtools
   Entry: OMICS_00903
 - Name: bio.tools
   Entry: artemis
 - Name: conda:bioconda
   Entry: artemis
Repository: https://github.com/sanger-pathogens/Artemis.git
Repository-Browse: https://github.com/sanger-pathogens/Artemis
